from django.shortcuts import redirect, render, reverse


def login_redirect(request):
    return redirect(reverse("home:home"))


def error_404(request, exception):
    data = {}
    return render(request, "errors/404.html", data)


def error_500(request):
    data = {}
    return render(request, "errors/500.html", data)
