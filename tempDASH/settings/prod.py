import os

from tempDASH.settings.base import *

# Overwrite base.py settings here


# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": "dashboard",
        "USER": os.getenv("DATABASE_USER"),
        "PASSWORD": os.getenv("DATABASE_PASSWORD"),
        "HOST": "localhost",
        "PORT": "",
    }
}


# Debug

DEBUG = False


try:
    from tempDASH.settings.local import *
except:
    pass
