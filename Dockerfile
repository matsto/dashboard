ARG sec_key
ARG mail_user
ARG mail_pass
ARG db_user
ARG db_pass
ARG admin_name
ARG admin_mail

FROM debian

RUN apt update
RUN apt -y dist-upgrade
RUN apt -y install apache2
RUN apt -y install python3
RUN apt -y install apt-utils
RUN apt -y install python3-venv
RUN apt -y install python3-dev python3-setuptools

RUN apt -y install libtiff5-dev libjpeg62-turbo-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev libharfbuzz-dev libfribidi-dev tcl8.6-dev tk8.6-dev python-tk
RUN mkdir /django/

ADD . /django

RUN python3 -m venv /django/venv
RUN . /django/venv/bin/activate

RUN pip3 install -r /django/requirements.txt

ENV SECRET_KEY $sec_key
ENV EMAIL_HOST smtp.gmail.com
ENV EMAIL_PORT 587
ENV EMAIL_HOST_USER $mail_user
ENV EMAIL_HOST_PASSWORD $mail_pass
ENV DATABASE_USER $db_user
ENV DATABASE_PASSWORD $db_pass
ENV ADMIN $admin_name
ENV ADMIN_EMAIL $admin_mail

RUN python3 /django/manage.py makemigrations home accounts data
RUN python3 /django/manage.py migrate

CMD . /django/venv/bin/activate

CMD celery -A tempDASH worker -B
CMD celery -A tempDASH beat -l info --scheduler django_celery_beat.schedulers:DatabaseScheduler

CMD apachectl -D FOREGROUND
CMD python3 /django/manage.py runserver 0.0.0.0:8585
