from django.contrib.auth.models import User
from django.db import models
from django.utils.crypto import get_random_string

# Create your models here.


class Device(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="devices"
    )
    device_id = models.CharField(max_length=17, unique=True)
    name = models.CharField(max_length=20, blank=False)
    alarm_day = models.PositiveSmallIntegerField(default=0)
    alarm_hour = models.PositiveSmallIntegerField(default=0)
    alarm_minute = models.PositiveSmallIntegerField(default=0)
    alarm_second = models.PositiveSmallIntegerField(default=0)
    last_timestamp = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return "{} - {}".format(self.device_id, self.name)

    def get_generated_name(self):
        random_device_id = get_random_string()
        self.device_id = random_device_id
        self.name = "Device_" + random_device_id
        return self
