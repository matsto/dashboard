import graphene
from graphene_django.types import DjangoObjectType

from device.models import Device


class DeviceType(DjangoObjectType):
    class Meta:
        model = Device


class Query(object):
    all_devices = graphene.List(DeviceType)

    def resolve_all_devices(self, info, **kwargs):
        return Device.objects.select_related("user").all()
