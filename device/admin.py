from django.contrib import admin

from device.models import Device
from data.models import Marker


class MarkerInline(admin.StackedInline):
    model = Marker
    can_delete = False
    verbose_name_plural = "Markers"
    fk_name = "device"


class DeviceAdmin(admin.ModelAdmin):
    inlines = (MarkerInline,)
    list_display = (
        "user",
        "device_id",
        "name",
        "get_marker_1",
        "get_name_1",
        "get_marker_2",
        "get_name_2",
        "get_marker_3",
        "get_name_3",
        "get_marker_4",
        "get_name_4",
        "get_marker_5",
        "get_name_5",
        "alarm_day",
        "alarm_hour",
        "alarm_minute",
        "alarm_second",
        "last_timestamp",
    )
    list_display_links = list_display
    list_select_related = ("user", "markers")
    search_fields = ("user", "device_id", "name")
    list_filter = ("device_id", "user")

    def get_marker_1(self, instance):
        if hasattr(instance, "markers"):
            return instance.markers.tag_1

    get_marker_1.short_description = "Tag 1"

    def get_name_1(self, instance):
        if hasattr(instance, "markers"):
            return instance.markers.name_1

    get_name_1.short_description = "Name 1"

    def get_marker_2(self, instance):
        if hasattr(instance, "markers"):
            return instance.markers.tag_2

    get_marker_2.short_description = "Tag 2"

    def get_name_2(self, instance):
        if hasattr(instance, "markers"):
            return instance.markers.name_2

    get_name_2.short_description = "Name 2"

    def get_marker_3(self, instance):
        if hasattr(instance, "markers"):
            return instance.markers.tag_3

    get_marker_3.short_description = "Tag 3"

    def get_name_3(self, instance):
        if hasattr(instance, "markers"):
            return instance.markers.name_3

    get_name_3.short_description = "Name 3"

    def get_marker_4(self, instance):
        if hasattr(instance, "markers"):
            return instance.markers.tag_4

    get_marker_4.short_description = "Tag 4"

    def get_name_4(self, instance):
        if hasattr(instance, "markers"):
            return instance.markers.name_4

    get_name_4.short_description = "Name 4"

    def get_marker_5(self, instance):
        if hasattr(instance, "markers"):
            return instance.markers.tag_5

    get_marker_5.short_description = "Tag 5"

    def get_name_5(self, instance):
        if hasattr(instance, "markers"):
            return instance.markers.name_5

    get_name_5.short_description = "Name 5"


admin.site.register(Device, DeviceAdmin)
