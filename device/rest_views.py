from rest_framework import viewsets
from rest_framework.response import Response

from api.permissions import IsOwner
from device.models import Device
from device.serializers import DeviceSerializer


class DeviceViewSet(viewsets.ModelViewSet):
    permission_classes = [IsOwner]
    queryset = Device.objects.all()
    serializer_class = DeviceSerializer
    filterset_fields = ["user"]

    def list(self, request):
        queryset = Device.objects.filter(user=self.request.user)
        serializer = DeviceSerializer(
            queryset, many=True, context={"request": request}
        )
        return Response(serializer.data)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
