# Generated by Django 2.2.3 on 2019-10-28 16:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("device", "0001_initial")]

    operations = [
        migrations.AlterField(
            model_name="device",
            name="name",
            field=models.CharField(max_length=20),
        )
    ]
