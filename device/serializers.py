from drf_queryfields import QueryFieldsMixin
from rest_framework import serializers

from device.models import Device
from rest_framework_nested.relations import NestedHyperlinkedRelatedField


class DeviceSerializer(
    QueryFieldsMixin, serializers.HyperlinkedModelSerializer
):
    datas = NestedHyperlinkedRelatedField(
        many=True,
        read_only=True,  # Or add a queryset
        view_name="data-detail",
        parent_lookup_kwargs={"device_pk": "device_id"},
    )
    saves = NestedHyperlinkedRelatedField(
        many=True,
        read_only=True,  # Or add a queryset
        view_name="save-detail",
        parent_lookup_kwargs={"device_pk": "device_id"},
    )

    markers = NestedHyperlinkedRelatedField(
        many=False,
        read_only=True,  # Or add a queryset
        view_name="marker-detail",
        parent_lookup_kwargs={"device_pk": "device_id"},
    )

    class Meta:
        model = Device
        fields = (
            "url",
            "datas",
            "markers",
            "saves",
            "user",
            "device_id",
            "name",
            "alarm_day",
            "alarm_hour",
            "alarm_minute",
            "alarm_second",
            "last_timestamp",
        )
        read_only_fields = ("user", "last_timestamp")
