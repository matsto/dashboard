from __future__ import absolute_import, unicode_literals

from datetime import datetime, timedelta, timezone

from celery import shared_task
from django.contrib.auth.models import User
from django.core.mail import send_mail

from data.models import Data
from device.models import Device
from tempDASH.settings import base as settings


@shared_task
def lack_of_data_email():
    users = User.objects.all()
    for user in users:
        try:
            device = Device.objects.get(user=user)
        except Device.DoesNotExist:
            continue
        try:
            data = Data.objects.filter(device=device).latest("timestamp")
        except Data.DoesNotExist:
            continue

        # create timedelta from users settings
        user_time_difference = timedelta(
            days=device.alarm_day,
            hours=device.alarm_hour,
            minutes=device.alarm_minute,
            seconds=device.alarm_second,
        )

        print(user_time_difference)

        # check if user set something in the settings
        if user_time_difference != timedelta(0):

            # check if alert was send due to this crash
            if device.last_timestamp != data.timestamp:

                # check how much time ago was last query to database
                now = datetime.now(timezone.utc)
                time_difference = now - data.timestamp

                # if time since last query is bigger
                # then set by user send an email

                if user_time_difference < time_difference:
                    device.last_timestamp = data.timestamp
                    device.save()
                    send_mail(
                        "[DASHboard] your device has crashed",
                        "Dear "
                        + str(user.username)
                        + ", your last data query was at "
                        + data.timestamp.strftime("%Y-%m-%d %H:%M:%S")
                        + ", what is "
                        + str(time_difference)
                        + " from now. We suspect that your device has crashed,\
                        or there is a problem with Internet connection.",
                        settings.EMAIL_HOST_USER,
                        [user.email],
                        fail_silently=False,
                    )
                else:
                    pass
            else:
                pass
        else:
            pass
