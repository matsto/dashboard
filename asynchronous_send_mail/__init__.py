from celery import shared_task
from django.core.mail import EmailMultiAlternatives


@shared_task
def email_celery(
    subject, body, from_email, recipient_list, fail_silently, html_message
):
    msg = EmailMultiAlternatives(subject, body, from_email, recipient_list)
    if html_message:
        msg.attach_alternative(html_message, "text/html")
    msg.send(fail_silently)


def send_mail(
    subject,
    body,
    from_email,
    recipient_list,
    fail_silently=False,
    html_message=None,
    *args,
    **kwargs,
):
    email_celery.delay(
        subject, body, from_email, recipient_list, fail_silently, html_message
    )
