from rest_framework import mixins, viewsets
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework_csv import renderers as r

from api.permissions import IsOwner
from data.serializers import DataSerializer, MarkerSerializer, SaveSerializer
from data.models import Data, Marker, Save
from device.models import Device


class DataViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = [IsOwner]
    queryset = Data.objects.all()
    serializer_class = DataSerializer
    filterset_fields = ["timestamp", "device"]
    renderer_classes = tuple(api_settings.DEFAULT_RENDERER_CLASSES) + (
        r.CSVRenderer,
    )

    def list(self, request, device_pk):
        queryset = Data.objects.filter(device_id=device_pk)
        serializer = DataSerializer(
            queryset, many=True, context={"request": request}
        )
        return Response(serializer.data)


class MarkerViewSet(viewsets.ReadOnlyModelViewSet, mixins.UpdateModelMixin):
    """
    A simple ViewSet for viewing accounts.
    """

    permission_classes = [IsOwner]
    queryset = Marker.objects.all()
    serializer_class = MarkerSerializer
    filterset_fields = ["device"]

    def list(self, request, device_pk):
        queryset = Marker.objects.filter(device_id=device_pk)
        serializer = MarkerSerializer(
            queryset, many=True, context={"request": request}
        )
        return Response(serializer.data)


class SaveViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing accounts.
    """

    permission_classes = [IsOwner]
    queryset = Save.objects.all()
    serializer_class = SaveSerializer
    filterset_fields = ["device"]

    def list(self, request, device_pk):
        devices = Device.objects.filter(pk=device_pk)
        queryset = Save.objects.filter(device__in=devices)
        serializer = SaveSerializer(
            queryset, many=True, context={"request": request}
        )
        return Response(serializer.data)
