from drf_queryfields import QueryFieldsMixin
from rest_framework import serializers
from rest_framework_nested.relations import (
    NestedHyperlinkedRelatedField,
    NestedHyperlinkedIdentityField,
)

from data.models import Data, Marker, Save


class DataSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):
    url = NestedHyperlinkedIdentityField(
        view_name="data-detail",
        parent_lookup_kwargs={"device_pk": "device_id", "pk": "pk"},
        read_only=True,
    )

    def to_representation(self, instance):
        from data.models import Marker

        ret = super().to_representation(instance)

        def get_marker_list(instance):
            marker_obj = Marker.objects.get(device=instance.device)
            marker_list = [
                (i + 1, getattr(marker_obj, "tag_{}".format(i + 1)))
                for i in range(5)
            ]
            return marker_list

        for index, marker in get_marker_list(instance):
            if marker:
                ret[marker] = ret.pop("value_{}".format(index))
            else:
                del ret["value_{}".format(index)]
        return ret

    class Meta:
        model = Data
        fields = "__all__"


class MarkerSerializer(
    QueryFieldsMixin, serializers.HyperlinkedModelSerializer
):
    url = NestedHyperlinkedIdentityField(
        view_name="marker-detail",
        parent_lookup_kwargs={"device_pk": "device_id", "pk": "pk"},
        read_only=True,
    )

    class Meta:
        model = Marker
        fields = "__all__"
        read_only_fields = ("tag_1", "tag_2", "tag_3", "tag_4", "tag_5")


class SaveSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):
    url = NestedHyperlinkedIdentityField(
        view_name="save-detail",
        parent_lookup_kwargs={"device_pk": "device_id", "pk": "pk"},
        read_only=True,
    )

    class Meta:
        model = Save
        fields = "__all__"
