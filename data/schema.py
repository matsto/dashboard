import graphene
from graphene_django.types import DjangoObjectType

from data.models import Data, Save, Marker


class DataType(DjangoObjectType):
    class Meta:
        model = Data


class MarkerType(DjangoObjectType):
    class Meta:
        model = Marker


class SaveType(DjangoObjectType):
    class Meta:
        model = Save


class Query(object):
    all_data = graphene.List(DataType)
    all_markers = graphene.List(MarkerType)
    all_saves = graphene.List(SaveType)

    def resolve_all_data(self, info, **kwargs):
        return Data.objects.select_related("device").all()

    def resolve_all_markers(self, info, **kwargs):
        return Marker.objects.select_related("device").all()

    def resolve_all_saves(self, info, **kwargs):
        return SaveType.objects.select_related("device").all()
