import logging
import os

from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from data.models import Data, Marker
from device.models import Device
from tempDASH.settings import base

logger = logging.getLogger(__name__)

log_file = os.path.join(base.BASE_DIR, "logs/db_saver.log")

f_handler = logging.FileHandler(log_file)
f_format = logging.Formatter(
    "%(asctime)s | %(name)s | %(levelname)s | %(message)s"
)
f_handler.setFormatter(f_format)
logger.addHandler(f_handler)


def db_saver(request, save_data):
    if request.method == "GET":
        indexes_and = [i for i, x in enumerate(save_data) if x == r"&"]
        indexes_equ = [i for i, x in enumerate(save_data) if x == r"="]

        # check if amount of +  and & is the same
        # what means that string is correct
        dev_id = save_data[0 : int(indexes_and[0])]

        # check if device with this id exist in database
        device_checker = get_object_or_404(Device, device_id=dev_id)
        if device_checker is None:
            logger.error(f"device doesn't exist | {save_data}")
            return HttpResponse(
                content="Error, device does not exist in the database",
                content_type="text/plain",
                status=403,
            )

        amount_and = len(indexes_and)
        amount_equ = len(indexes_equ)

        if amount_and != amount_equ or amount_and == 0 or amount_equ == 0:
            logger.error(f"string corrupted | {save_data}")
            return HttpResponse(
                content="Error, sent string is corrupted",
                content_type="text/plain",
                status=403,
            )

        string_length = len(save_data)

        marker_kwargs = {}
        data_kwargs = {}

        # run loop through string to recognize all values and tags
        for x in range(amount_and):

            # try to get tag
            try:
                mark = save_data[int(indexes_and[x]) + 1 : int(indexes_equ[x])]

                # save only if not null
                if mark != "":
                    marker_kwargs[
                        "tag_{}".format(x + 1)
                    ] = mark  # save result in tag list

            except IndexError:
                mark = save_data[int(indexes_and[x]) + 1 : string_length]

                # save only if not null
                if mark != "":
                    marker_kwargs[
                        "tag_{}".format(x + 1)
                    ] = mark  # save result in tag list

            # try to get value
            try:
                value = save_data[
                    int(indexes_equ[x]) + 1 : int(indexes_and[x + 1])
                ]

                # save only if not null
                if value != "":
                    data_kwargs[
                        "value_{}".format(x + 1)
                    ] = value  # save result in value list

            except IndexError:
                value = save_data[int(indexes_equ[x]) + 1 : string_length]

                # save only if not null
                if value != "":
                    data_kwargs[
                        "value_{}".format(x + 1)
                    ] = value  # save result in value list

        if len(marker_kwargs.values()) != len(set(marker_kwargs.values())):
            logger.error(f"tags ambiguous | {save_data}")
            return HttpResponse(
                content="Error, tags should be unique",
                content_type="text/plain",
                status=403,
            )
        Data.objects.create(device=device_checker, **data_kwargs)
        Marker.objects.update_or_create(
            device=device_checker, defaults=marker_kwargs
        )

        # get amount of data for device
        data_count = Data.objects.filter(device=device_checker).count()
        return HttpResponse(
            f"saved {data_count}", content_type="text/plain", status=200
        )
