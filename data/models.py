from django.db import models

from device.models import Device


class Data(models.Model):
    device = models.ForeignKey(
        Device, on_delete=models.CASCADE, related_name="datas"
    )
    value_1 = models.FloatField(null=True)
    value_2 = models.FloatField(null=True)
    value_3 = models.FloatField(null=True)
    value_4 = models.FloatField(null=True)
    value_5 = models.FloatField(null=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "[{}] [{}] {}".format(
            self.device.user.username, self.device.device_id, self.timestamp
        )

    class Meta:
        unique_together = ["device", "timestamp"]


class Save(models.Model):
    device = models.ForeignKey(
        Device, on_delete=models.CASCADE, related_name="saves"
    )
    title = models.CharField(max_length=20, blank=True)
    description = models.TextField(max_length=140, blank=True)
    date_from = models.DateField()
    date_to = models.DateField()

    def __str__(self):
        return "[{}] {}".format(self.device.device_id, self.title)


class Marker(models.Model):
    device = models.OneToOneField(
        Device, on_delete=models.CASCADE, related_name="markers"
    )
    tag_1 = models.CharField(max_length=4, blank=True, null=True)
    name_1 = models.CharField(max_length=20, blank=True)
    tag_2 = models.CharField(max_length=4, blank=True, null=True)
    name_2 = models.CharField(max_length=20, blank=True)
    tag_3 = models.CharField(max_length=4, blank=True, null=True)
    name_3 = models.CharField(max_length=20, blank=True)
    tag_4 = models.CharField(max_length=4, blank=True, null=True)
    name_4 = models.CharField(max_length=20, blank=True)
    tag_5 = models.CharField(max_length=4, blank=True, null=True)
    name_5 = models.CharField(max_length=20, blank=True)

    chart_title = models.CharField(blank=True, max_length=20)
    days_amount = models.IntegerField(default=3)

    def __str__(self):
        return "[{}] {}".format(self.device.device_id, self.chart_title)
