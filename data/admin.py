from django.contrib import admin

from data.models import Data, Save


class DataAdmin(admin.ModelAdmin):
    list_display = (
        "get_device",
        "timestamp",
        "value_1",
        "value_2",
        "value_3",
        "value_4",
        "value_5",
    )
    list_display_links = list_display
    list_select_related = ("device",)
    search_fields = ("device", "timestamp")
    list_filter = ("device", "device__user")

    def get_device(self, instance):
        if hasattr(instance, "device"):
            return instance.device.device_id

    get_device.short_description = "Device"


class SaveAdmin(admin.ModelAdmin):
    list_display = (
        "get_device",
        "title",
        "description",
        "date_from",
        "date_to",
    )
    list_display_links = list_display
    list_select_related = ("device",)
    search_fields = ("device", "title", "description")
    list_filter = ("device", "device__user")

    def get_device(self, instance):
        if hasattr(instance, "device"):
            return instance.device.device_id

    get_device.short_description = "Device"


admin.site.register(Save, SaveAdmin)
admin.site.register(Data, DataAdmin)
