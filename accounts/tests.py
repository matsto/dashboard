from django.contrib.auth.models import User
from django.db import IntegrityError
from django.test import TestCase


class TestRegister(TestCase):

    # test of the model User

    def setUp(self):
        self.user = User.objects.create(
            username="test", email="w@p2.com", password="Rom123asdsd"
        )

    def test_register_model(self):
        # basic positive test
        self.assertTrue(isinstance(self.user, User))

    def test_same_username(self):
        # test with the same username
        with self.assertRaises(IntegrityError):
            User.objects.create(
                username="test", email="a2@p2.com", password="Rosdm123asd"
            ), User
