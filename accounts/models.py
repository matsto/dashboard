from hashlib import md5

from django.contrib.auth.models import User
from django.db import models

import pytz


def get_all_timezones():
    tz_list = pytz.common_timezones
    return list(zip(tz_list, tz_list))


class UserProfile(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name="userprofile"
    )
    about_me = models.TextField(max_length=140, blank=True, default="")
    time_zone = models.CharField(
        max_length=30, choices=get_all_timezones(), default="UTC"
    )
    image = models.ImageField(upload_to="profile_image", blank=True)

    def __str__(self):
        return self.user.username

    @classmethod
    def avatar(cls, user, size):
        digest = md5(user.email.lower().encode("utf-8")).hexdigest()

        return "https://www.gravatar.com/avatar/{}?d=identicon&s={}".format(
            digest, size
        )
