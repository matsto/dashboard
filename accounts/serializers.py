from django.contrib.auth.models import User
from drf_queryfields import QueryFieldsMixin
from rest_framework import serializers

from rest_framework_nested.relations import (
    NestedHyperlinkedRelatedField,
    NestedHyperlinkedIdentityField,
)
from accounts.models import UserProfile


class UserSerializer(QueryFieldsMixin, serializers.HyperlinkedModelSerializer):
    userprofile = NestedHyperlinkedRelatedField(
        many=False,
        read_only=True,  # Or add a queryset
        view_name="userprofile-detail",
        parent_lookup_kwargs={"user_pk": "user_id"},
    )

    class Meta:
        model = User
        fields = (
            "url",
            "username",
            "first_name",
            "last_name",
            "email",
            "date_joined",
            "userprofile",
        )
        read_only_fields = ("date_joined",)


class UserProfileSerializer(
    QueryFieldsMixin, serializers.HyperlinkedModelSerializer
):

    url = NestedHyperlinkedIdentityField(
        many=False,
        read_only=True,  # Or add a queryset
        view_name="userprofile-detail",
        parent_lookup_kwargs={"user_pk": "user_id", "pk": "pk"},
    )

    class Meta:
        model = UserProfile
        fields = "__all__"
        read_only_fields = ("user",)


class PasswordSerializer(serializers.Serializer):
    password = serializers.CharField(max_length=50)
