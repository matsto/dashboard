from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from .models import UserProfile


class ProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False
    verbose_name_plural = "Profiles"
    fk_name = "user"


class CustomUserAdmin(UserAdmin):
    inlines = (ProfileInline,)
    list_display = (
        "username",
        "email",
        "first_name",
        "last_name",
        "is_staff",
        "get_about_me",
        "get_time_zone",
    )
    list_display_links = list_display
    list_select_related = ("userprofile",)
    search_fields = ("username", "email")
    list_filter = ("is_staff", "is_active", "is_superuser")

    def get_about_me(self, instance):
        if hasattr(instance, "userprofile"):
            return instance.userprofile.about_me

    get_about_me.short_description = "About me"

    def get_time_zone(self, instance):
        if hasattr(instance, "userprofile"):
            return instance.userprofile.time_zone

    get_time_zone.short_description = "Timezone"

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(CustomUserAdmin, self).get_inline_instances(request, obj)


admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
