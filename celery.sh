#test variables

export PYTHONPATH="."
export DJANGO_SETTINGS_MODULE='tempDASH.settings.dev'
export SECRET_KEY='secret_key_3kr'
export EMAIL_HOST='smtp.gmail.com'
export EMAIL_PORT=587
export EMAIL_HOST_USER='user@gmail.com'
export EMAIL_HOST_PASSWORD='pass'
export DATABASE_USER='db_user'
export DATABASE_PASSWORD='db_pass'
export ADMIN="admin"
export ADMIN_EMAIL="user@gmail.com"

# personal variables

#create a file to overwrite above variables
. ./local_env_var.sh

# overwrite variables
if [ $PYTHONPATH ]
then
    export PYTHONPATH=$PYTHONPATH
fi

if [ $DJANGO_SETTINGS_MODULE ]
then
    export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
fi

if [ $SECRET_KEY ]
then
    export SECRET_KEY=$SECRET_KEY
fi

if [ $EMAIL_HOST_USER ]
then
    export EMAIL_HOST_USER=$EMAIL_HOST_USER
fi

if [ $EMAIL_HOST_PASSWORD ]
then
    export EMAIL_HOST_PASSWORD=$EMAIL_HOST_PASSWORD
fi

if [ $DATABASE_USER ]
then
    export DATABASE_USER=$DATABASE_USER
fi

if [ $DATABASE_PASSWORD ]
then
    export DATABASE_PASSWORD=$DATABASE_PASSWORD
fi

if [ $ADMIN ]
then
    export ADMIN=$ADMIN
fi

if [ $ADMIN_EMAIL ]
then
    export ADMIN_EMAIL=$ADMIN_EMAIL
fi

echo $SECRET_KEY

. ./venv/bin/activate
celery flower -A tempDASH --address=127.0.0.1 --port=5555 &
celery -A tempDASH worker -B &
celery -A tempDASH beat -l info --scheduler django_celery_beat.schedulers:DatabaseScheduler &
