import graphene
import device.schema
import data.schema


class Query(data.schema.Query, device.schema.Query, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query)
