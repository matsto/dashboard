Readme dated to **09.03.2020**

# DASHboard

## Description:

The application has ability to collect and store data in the PostgreSQL database. Collected data may be presented to the user through front-end client integrated with app's API.

The application gives possibility to create an user and to mange them. A created user may have as many devices as them wants.

The backend has integration with OAuth (Google, Facebook) for easier authentication.

### Dependies

Be aware that the Dashboard uses redis, to get celery run install:

#### For Ubuntu or Debian:

1. Install Dependies
    `sudo apt install -y redis`
2. Run server
    `systemctl enable redis-server`
    `systemctl start redis-server`
3. Check if server is working
    `systemctl status redis-server`

## Disclaimer:

If you use `make install` the **test** user will be created, which is suitible to get data from the resources_monitor app.
- login: test
- pass: testtest!@#

## How to:
* `make install` – To run the application for the very first time. It will install and run everything for you, just open `127.0.0.1:8000` in your favorite browser.
* `make run` – This is basic method to run the app once it’s installed.
* `make test` – to run unit tests
