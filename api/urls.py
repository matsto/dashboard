from django.urls import include, path, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from graphene_django.views import GraphQLView
from rest_framework import permissions
from rest_framework_nested import routers
from rest_framework_simplejwt import views as jwt_views

from accounts.rest_views import (
    CurrentUserViewSet,
    UserProfileViewSet,
    UserViewSet,
)
from data.rest_views import DataViewSet, MarkerViewSet, SaveViewSet
from device.rest_views import DeviceViewSet

root = routers.DefaultRouter()

root.register("devices", DeviceViewSet)

device_router = routers.NestedDefaultRouter(root, r"devices", lookup="device")
device_router.register("datas", DataViewSet)
device_router.register("markers", MarkerViewSet)
device_router.register("saves", SaveViewSet)

root.register("users", UserViewSet)
user_router = routers.NestedDefaultRouter(root, r"users", lookup="user")
user_router.register(
    "userprofiles", UserProfileViewSet, basename="userprofile"
)

root.register("current_user", CurrentUserViewSet, basename="current")


schema_view = get_schema_view(
    openapi.Info(
        title="Snippets API",
        default_version="v1",
        description="Test description",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="contact@snippets.local"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)


swagger_urls = [
    re_path(
        r"^swagger(?P<format>\.json|\.yaml)$",
        schema_view.without_ui(cache_timeout=0),
        name="schema-json",
    ),
    re_path(
        r"^swagger/$",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    re_path(
        r"^redoc/$",
        schema_view.with_ui("redoc", cache_timeout=0),
        name="schema-redoc",
    ),
]

simple_jwt_urls = [
    path(
        "token/",
        jwt_views.TokenObtainPairView.as_view(),
        name="token_obtain_pair",
    ),
    path(
        "token/refresh/",
        jwt_views.TokenRefreshView.as_view(),
        name="token_refresh",
    ),
]

urlpatterns = [
    path("", include("social_django.urls")),
    path("", include(root.urls)),
    path("", include(device_router.urls)),
    path("", include(user_router.urls)),
    path("", include(swagger_urls)),
    path("", include(simple_jwt_urls)),
    path("graphql/", GraphQLView.as_view(graphiql=True)),
    path(
        "users/<int:user_pk>/profiles/",
        UserProfileViewSet.as_view({"get": "list", "post": "create"}),
    ),
    path(
        "users/<int:user_pk>/profiles/<int:pk>/",
        UserProfileViewSet.as_view(
            {"get": "retrieve", "post": "update", "delete": "destroy"}
        ),
    ),
    path("auth/", include("rest_framework_social_oauth2.urls")),
]
