from rest_framework import permissions

from device.models import Device


class IsOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        user = request.user
        devices = Device.objects.filter(user=user)

        if hasattr(obj, "user"):
            return obj.user == user
        elif hasattr(obj, "device"):
            return obj.device in devices
        else:
            return obj == user
