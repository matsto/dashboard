HOST:=127.0.0.1
PORT:=8000
VENV_BIN_DIR:="./venv/bin"
CMD_FROM_VENV:=". $(VENV_BIN_DIR)/activate; which"
PYTHON=$(shell "$(CMD_FROM_VENV)" "python")
PIP:="$(VENV_BIN_DIR)/pip"
REQUIREMENTS:="requirements.txt"

export PYTHONPATH="."
export DJANGO_SETTINGS_MODULE='tempDASH.settings.prod'
export SECRET_KEY='secret_key_3kr'
export EMAIL_HOST='smtp.gmail.com'
export EMAIL_PORT=587
export EMAIL_HOST_USER='user@gmail.com'
export EMAIL_HOST_PASSWORD='pass'
export DATABASE_USER='db_user'
export DATABASE_PASSWORD='db_pass'
export ADMIN="admin"
export ADMIN_EMAIL="user@gmail.com"

# overwrite with personal variables
include local_env_var.sh
export $(shell sed 's/=.*//' local_env_var.sh)

.PHONY: all venv celery test run install superuser dumpdata

all:
	echo make [OPTION]
	echo
	echo OPTIONS:
	echo
	echo install - installation of virtual environment, all requirements. After installation load fixtures and run a dev-server.
	echo run - starts virtual environment and a dev-server. It doesn\'t install virtual environment and requirements.
	echo test - run tests of the app
	echo superuser - create superuser
	echo dumpdata - dump database for further use.
	
venv: 
	python3 -m venv venv
	@$(PIP) install -r $(REQUIREMENTS)


.ONESHELL:
celery: 
	@sh ./celery.sh

test: 
	@$(PYTHON) manage.py test

run:
	make celery
	@$(PYTHON) manage.py runserver $(HOST):$(PORT)

install: venv
	@$(PYTHON) manage.py makemigrations
	@$(PYTHON) manage.py migrate
	@$(PYTHON) manage.py loaddata fixtures.json
	make celery
	@$(PYTHON) manage.py runserver $(HOST):$(PORT)

superuser: 
	@$(PYTHON) manage.py createsuperuser

dumpdata:
	@$(PYTHON) manage.py dumpdata --exclude=data --output=fixtures.json
